This repo is about an app whereby we have common bad practices for Angular developers. This repo is associated with the brownbag "Do's and Dont's - Angular"

### TODOs

- [ ] Create feature modules and lazy-load

* posts module
* user module

```js
loadChildren: () =>
  import('path_to_module').then(m => m.ModuleName),
```

- [ ] Create reusable module and card

```sh
ng g m shared
ng g c shared/card --export
```

- [ ] Use RxJs map Operator for users

* mergeMap Operator in user page

```js
this.subscription$.pipe(takeUntil(this.destroyed$), map(val -> val.username))
```

- [ ] Clean-up any open subscriptions

```js
this.subscription$.pipe(takeUntil(this.destroyed$));
this.subscription$.pipe(takeUntilDestroyed());
this.subscriptions.push(this.subscription$);
```
