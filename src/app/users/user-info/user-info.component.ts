import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngbb-user-info',
  templateUrl: './user-info.component.html',
  styleUrl: './user-info.component.css',
})
export class UserInfoComponent {
  public user!: any;
  public userPosts!: any[];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private httpClient: HttpClient
  ) {
    const userId = this.activatedRoute.snapshot.paramMap.get('id');

    if (userId) {
      this.httpClient
        .get<any>(`https://jsonplaceholder.typicode.com/users/${userId}`)
        .subscribe({
          next: (val) => (this.user = val),
        });

      this.httpClient
        .get<any[]>(
          `https://jsonplaceholder.typicode.com/posts?userId=${userId}`
        )
        .subscribe({
          next: (posts) => (this.userPosts = posts),
        });
    }
  }
}
