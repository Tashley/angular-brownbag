import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngbb-user-list',
  templateUrl: './user-list.component.html',
  styleUrl: './user-list.component.css',
})
export class UserListComponent {
  public allUsers!: any[];

  constructor(private httpClient: HttpClient, private readonly router: Router) {
    this.httpClient
      .get<any[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe({
        next: (val) => (this.allUsers = val),
      });
  }

  public viewUser(userId: string): void {
    this.router.navigate([`/user/${userId}`]);
  }
}
