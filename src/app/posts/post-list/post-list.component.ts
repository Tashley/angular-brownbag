import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngbb-post-list',
  templateUrl: './post-list.component.html',
  styleUrl: './post-list.component.css',
})
export class PostListComponent implements OnInit {
  allPosts: any[] = [];

  constructor(
    private readonly httpClient: HttpClient,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.httpClient
      .get<any[]>('https://jsonplaceholder.typicode.com/posts')
      .subscribe({
        next: (val: any[]) => {
          this.allPosts = val;
        },
      });
  }

  public viewUser(userId: string): void {
    this.router.navigate([`/user/${userId}`]);
  }
}
