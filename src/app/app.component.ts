import { Component } from '@angular/core';

@Component({
  selector: 'ngbb-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'angular-brownbag-modules';
}
